\documentclass[a4paper,10pt]{report}
\usepackage[utf8]{inputenc}
%\usepackage[swedish]{babel}
\usepackage{framed}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[round]{natbib}
%\usepackage{listings}

% user defined commands
\newcommand{\code}[1]{\medskip \texttt{#1} \medskip}
\newcommand{\approxtext}[1]{\ensuremath{\stackrel{\ \text{\footnotesize#1}\ }{\approx}}}
\newcommand{\equaltext}[1]{\ensuremath{\stackrel{\ \text{\footnotesize#1}\ }{=}}}

% Title Page
\title{ Visual PyFlow user manual}
\author{Andrew Frampton, Benoît Dessirier, Romain Pannetier}

\begin{document}
\maketitle

\section*{Background}

Visual PyFlow is a simple model builder to solve the groundwater flow equation. 
It is conceptually simple and currently only handles steady state saturated flow  
but the flexible user interface provides an integrated solution for creating/visualizing both user inputs and results. 
It allows the user to quickly set up and run simulations to answer theoretical questions without having to parse input data or postprocess 
results with external softwares. 
It is therefore appropriate for teaching purposes.

\section*{Model theory}
Visual PyFlow solves the \textbf{steady state saturated groundwater flow equation} using a finite volume numerical method.
This equation is based on the physical principle of mass conservation and on the constitutive equation driving fluid flow in porous media also known as Darcy's law.

  \subsection*{Governing equation}
    \subsubsection*{Mass conservation’s equation}
    
    This equation originates in the simple idea that, 
    assuming that no water is removed nor injected into the system, 
    the mass of water in the system should remain constant over time.
    %It is also assumed that water is incompressible.
    \smallskip
    
    Consider the apparent velocity of water in the $y$ direction as it enters the system
    (left side on figure \ref{fig:domain}), 
    further noted as $q_y$. 
    This velocity is directly related to flow in such 
    way that flow is the product of the apparent velocity and the
    area of the section that the water passes through. 
    With the conventions introduced in figure \ref{fig:domain}, 
    we have:
    
    \begin{equation}
    Q_y = q_y \cdot  \Delta x \Delta z 
    \label{eq:flow_velocity_relation}
    \end{equation}
    Where $Q_y$ is the volumetric discharge along the $y$-direction.
    
    \bigskip
    \begin{figure}[htb]
      \centering
      \input{domain.pdf_tex}
    \caption{Simple rectangular domain}
    \label{fig:domain}
    \end{figure}
    \bigskip
    
    Water exits the system with $q_y$ plus a change in velocity $\frac{\partial q_y}{\partial y}$ over $\Delta y$. 
    The net difference in velocity in the $y$-direction thus is $\frac{\partial q_y}{\partial y} \cdot \Delta y$ 
    and according to equation \ref{eq:flow_velocity_relation}, the net difference in mass becomes:
    $\rho\cdot \left( \frac{\partial q_y}{\partial y} \cdot \Delta y \right) \cdot \Delta x  \Delta z$ where $\rho$ is the density of water.
    We can rewrite it $\rho\cdot \frac{\partial q_y}{\partial y} \cdot \Delta V$,
    where $\Delta V$ is the volume of the domain.
    Our assumption that the mass remains unchanged over time implies that the sum of flow
    in the three directions must be equal to zero. And if we assume that liquid water is incompressible (i.e $\rho$ is constant) 
    we can write:
      
    \begin{equation}
    \frac{\partial q_x}{\partial x} \cdot \Delta V + 
    \frac{\partial q_y}{\partial y} \cdot \Delta V +
    \frac{\partial q_z}{\partial z} \cdot \Delta V = 0
    \label{eq:sum_flow_zero}
    \end{equation}
    or equivalently :
   
    \begin{equation}
    \frac{\partial q_x}{\partial x} + 
    \frac{\partial q_y}{\partial y} +
    \frac{\partial q_z}{\partial z} =
    \nabla\cdot q=0
    \label{eq:sum_partials_zero}
    \end{equation}
    where $\nabla\cdot q$ is called the divergence of the vector $q$.
   
   
    \subsubsection*{Darcy’s law}
    
    In the above section, we mentioned a velocity $q$ and specified 
    that it was the discharge per unit cross sectional area. 
    We called it \textquotedblleft apparent\textquotedblright because it is distinct 
    from the actual velocity of water particles at a microscopic level. 
    Indeed, individual particles follow complex flow-path 
    that depend on the effective porosity of the material.
    The apparent velocity $q$ refers on the other hand to an average linear velocity
    on a macroscopic level. 
    It is often referred to as \textbf{specific discharge} or even \textbf{Darcy velocity} and 
    can be calculated using \textbf{Darcy's law}.
    
    To express Darcy's law, we need to define the hydraulic head, further noted $h$, as a measure of the total energy
    that a particle of water currently holds both as gravitationnal potential energy and as internal pressure energy 
    (the kinetic energy is neglected with regards to the relatively low velocities occurring in underground flow conditions).
    One way to practically picture the local hydraulic head $h$ is to imagine that you stick a vertical pipe at the point
    you want to measure $h$ and read the height that the water reaches in the pipe. Darcy's law relates the specific discharge and the hydraulic head field as follows:
    
    \medskip
    % first column
    \begin{framed} %%%%%%%%%%%%%%%%%%%%%%%%%%% Darcy's law in frame %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% first column
    \begin{minipage}[t]{0.3\textwidth}
    \centering
    \textbf{Darcy's law}
    \bigskip
    \begin{equation}
    q = -K \cdot \frac{dh}{dl}	
    \label{eq:darcy}
    \end{equation}
    \end{minipage}
    \begin{minipage}[t]{0.1\textwidth}
        \ 
    \end{minipage}
    \begin{minipage}[t]{0.52\textwidth}   
    \begin{framed} %% Legend
    $q$ : Darcy's velocity (ms\textsuperscript{-1}) \\
    $K$ : Hydraulic conductivity (ms\textsuperscript{-1})\\
    $h$ : Hydraulic head(m) \\
    $l$ : length (m) \\
    $\frac{dh}{dl}$ : Change in head over length
    \end{framed}%%
    \end{minipage}
    \end{framed}%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end of Darcy %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
    The negative sign is due to the fact that water flows towards the lower hydraulic head, in other words for example: at constant altitude, 
    water flows from high pressure zones to low pressure zones and at constant pressure conditions, water flows from high
    altitude to lower altitude.
    The Darcy velocity also expresses that the discharge is proportional to the head gradient.
    In a porous medium like a soil or a permeable rock, the coefficient of proportionality $K$ is called hydraulic conductivity 
    and is a function of the material's intrinsic permeability $k$ 
    that describe its ability to let water go through.
    
    \begin{equation}
     K = k \cdot \frac{\rho g}{\mu}
    \end{equation}
    Where $\rho$ denotes water's density, $\mu$ its viscosity and
    $g$ is the gravitational acceleration.
     
    \medskip
  \subsection*{numerical approach}
    NB : Since Visual PyFlow is isothermal (temperature remains constant)
    and water is considered incompressible,
    the hydraulic conductivity is directly proportional to the permeability.
    The software allows users to assign any values $K$ 
    to different materials, but it should be kept in mind that 
    hydraulic conductivity is not a property of the material per se. 
    
    \subsubsection*{Saturated groundwater flow equation }
    
    The governing equation in Visual PyFlow is derived from
    the combination of mass conservation equation and Darcy's law.
    Since this law applies in all three dimensions, $q$ is a vector, it has therefore
    horizontal, vertical and depth components:
   
    \begin{align}
    q &= ( q_x , q_y , q_z) \notag\\ 
    q_x = -K \frac{\partial h}{\partial z} \ \ ; \ \ q_y &= -K \frac{\partial h}{\partial y}  
    \ \ ; \ \ q_z = -K \frac{\partial h}{\partial z}
    \end{align}
    Inserting these into equation \ref{eq:sum_partials_zero}, we obtain:
    
    \begin{align}
    \frac{\partial}{\partial x} \left(-K \frac{\partial h}{\partial x}\right) + 
    \frac{\partial}{\partial y} \left(-K \frac{\partial h}{\partial y}\right) + 
    \frac{\partial}{\partial z} \left(-K \frac{\partial h}{\partial z}\right) +  = 0
    \end{align}
    or, in cases where $K$ can be regarded as a constant:
    
    \begin{align}
    \frac{\partial ^2 h}{\partial x^2} + 
    \frac{\partial ^2 h}{\partial y^2} + 
    \frac{\partial ^2 h}{\partial z^2} = 0
    \end{align}
    
\newpage
  \subsection*{Numerical methods}
    \subsubsection*{Numerical solution to the steady state saturated groundwater flow equation}
    Visual PyFlow uses a pre-existing general purpose partial differential equation (PDE) solver called FiPy \citep{guyer2009fipy}. FiPy is a python library
    that allows to quickly:
    \begin{itemize}
    \item define a simple geometry,
    \item discretize it to obtain a computational mesh,
    \item assemble custom terms into differential equations describing the physics at stake,
    \item define the required initial and/or boundary conditions,
    \item and finally solve the problem.
    \end{itemize}
    Fipy uses the cell-centered finite volume method. The geometry is decomposed into elementary cells 
    in which all variables and parameters are further-on assumed to have a homogeneous value. The local mass conservation equation (\ref{eq:sum_partials_zero})
    is first integrated over each cell. And then the volume integrals of the divergence term is transformed into a surface integral according
    to the divergence theorem which states that for any vector $\overrightarrow{q}$, over a closed volume $V$ (here our cells) delimited 
    by the surface $S$:
    \begin{align}
      \iiint _V \nabla\cdot \overrightarrow{q} \ \mathrm{d}V \equaltext{divergence theorem} \iint _S \overrightarrow{q}\cdot \overrightarrow{\mathrm{d}S}
       \equaltext{mass conservation} 0 
    \label{eq:divthm}
    \end{align}
    where $\overrightarrow{\mathrm{d}S}$ is the local normal vector to the surface $S$.
    and if we decompose $S$ as $S=\bigcup S_{mn}$ where $S_{mn}$ is the contact area between two cells $m$ and $n$:
    \begin{align}
    \iint _S \overrightarrow{q}\cdot \overrightarrow{\mathrm{d}S} = \sum\limits_{S_{mn} \in S} \iint _{S_{mn}} \overrightarrow{q}\cdot \overrightarrow{\mathrm{d}S}
    \end{align}

    
    The groundwater volumetric flux $Q_{mn}$ between two cells $m$ and $n$ is expressed by Darcy's law (\ref{eq:darcy}) and approximated as:
    \begin{align}
    Q_{mn} = \iint _{S_{mn}} \overrightarrow{q}\cdot \overrightarrow{\mathrm{d}S} \approxtext{finite volume} S_{mn}\cdot q_{mn} \equaltext{Darcy's law} S_{mn}\cdot K_{mn}\frac{h_n-h_m}{D_{mn}}
    \end{align}
    where $q_{mn}$ is the discharge from $m$ to $n$, $S_{mn}$ is the contact area between the two cells, $D_{mn}$ is the distance between the centers of the two cells and
    $K_{mn}$ is the harmonic mean of $K_m$ and $K_n$.%to check later
    The boundaries of the domain simply act as extra interfaces where we directly specify the volumetric flux or a prescribed hydraulic head.
    
    By collecting all the notations we have just introduced, we obtain:
    \begin{align}
    \forall m, \sum\limits_{n\neq m} S_{mn}\cdot K_{mn}\frac{h_n-h_m}{D_{mn}} = 0
    \end{align}
    We thus have a system of equations that we can solve to obtain the head field.
  
    \subsubsection*{Pollock flow-lines}
    Visual PyFlow lets users visualize the flow-path that goes through a given point of their choice. 
    
    The finite volume method provides a cell-based approximation of the head field. The flow field is thus not directly accessible
    but instead we know the total water amounts that flow accross certain interfaces between cells. Thus to obtain approximate flow lines we need
    to interpolate the trajectories inside each cell.
    Visual PyFlow determines these path lines using the method described by \citet*{pollock_semianalytical_1988}.
    It interpolates trajectories inside each cell knowing the flow values at each interface the cell has with its neighbors by assuming a linear
    variation of the discharge both along $x$ and $y$.
    Thus for any point inside a rectangular cell it can determine the point at which a particle exits (or has entered) 
    the cell and which is the next (previous) cell it will enter (comes from).
    Additionally, Pollock's method calculates the position within cells of a particle at regular time intervals.    

        
    
\section*{Installation}
Visual PyFlow is an open source software, 
which mean that you are free to download the source code,
study it, run it, and improve it. 
The program requires no installation and can be run on any plattform,
you only need a python interpreter.

\subsection*{Run from binary executable (Windows only)}

Windows user may be intersted in a quick start and use the executable file. 
Download the executable file Visual\_PyFlow.exe Under the "Download" thumbnail. 
Doublebleclick it, you are now up and running. 
Start by oppening a new project (file > New) or oppen an existing one (file > Oppen).

\subsection*{Interpret from source}

Python is an interpreted languange and 
as such it can be run as a script without compilation and without installation. 
This works on any platform, 
however some prepartions need to be made, 
and a number of dependencies must be installed:

\begin{enumerate}
 \item Python: 
 
  Check that you have Python 2.7 installed as default on your system.
  
  \medskip
  \textbf{Windows} : 
  
  It is usually in 'C:\\Python27' If you can not find it, an installer (python-2.7.5.msi) is provided in the "installers\_win32" folder under "Download".

  
  \medskip
  \textbf{Linux} : 
  
  To verify which version you have, enter

  
  \code{python --version}

    
  If needed install a new version, start by running:

  \code{
  sudo apt-get install build-essential \\
  sudo apt-get install libreadline-dev libncursesw5-dev ... \\
    ...libssl-dev libsqlite3-dev tk-dev libgdbm-dev ... \\
    ... libc6-dev libbz2-dev  } 
     
  Then download, extract and install using the following commands:

  \code{
  wget http://python.org/ftp/python/2.7.5/Python-2.7.5.tgz \\
  tar -xvf Python-2.7.5.tgz \\                                    
  cd Python-2.7.5 \\
  ./configure \\
  make \\
  sudo make altinstall \\  }

 \item Required python modules:

    Fipy 3.0
    h5py 2.1
    matplotlib 1.2
    numpy 1.7
    Pyside 1.1
    pysparse 1.1
    scipy 0.12

  \textbf{Windows}: 
  
  To make it easier,
  we gathered the setups of the above listed module in the "installers\_win32" folder under "Download". 
  You can run them successively or double-click on the file 'install.py'. 
  Then, execute and walk through the opening setup menus, one at a time.
  You should have a report popping up with successful completion of the installation process.

  \textbf{Linux}: 
  
  You will have to browse the internet to find all the above-listed dependencies.
  They might have different installation procedures. 
  Yes, it will be time-consuming, no,
  it will not be fun. You can save time by installing the Enthought Python Distribution,
  which includes most of the needed modules, 
  but you will have to be careful not to get confused when you have several pyhton installer on the same computer.

\end{enumerate}
Finally, to run the program as a script, ensure these four things are in the same repertory:

   \begin{itemize}
   
    \item Visual\_PyFlow.py
    \item saturated\_gwflow\_solver.py
    \item pollock.py
    \item the images folder

   \end{itemize}

If you are using Enthought or if for another reason you could not install Fipy, 
include the "FiPy-3.0" folder in the same repertory. 
Run the script Visual\_PyFlow.py by double clicking it or open a command line / terminal and enter

\code{python Visual\_PyFlow.pyw}

You are up and running. Start by oppening a new project (file > New) or oppen an existing one (file > Oppen).
















\section*{Test cases}
\subsection*{Case 1: homogeneous\_confined.h5 }
This case is a simple simulation of an homogeneous confined aquifer. 
Single values are used for hydraulic conductivity and porosity
throughout the domain (homogeneous) and no flow takes place through the 
top and bottom (confined). The hydraulic head is set to 150m on the left hand
side and to 50m on the right hand side, which according to 
Darcy’s law will lead to a left-to-right migration of water (see equation \ref{eq:darcy}). 
You can think of this domain as an horizontal layer of conductive sediments between two impermeable layers,
lying underneath a hillslope. 
At the top of the hill, on the left hand side,
the column of water is maintained high by constant rain, and as with a water tower, 
the water migrates downhill through our domain whose permeability allows it.

\medskip
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.9\textwidth]{homogeneous_confined}
  \caption{Homogeneous confined aquifer in Visual Pyflow}
\end{figure}
\medskip

\subsection*{Case 2: vertically\_layered\_confined.h5}
This second case represents a confined flow in a vertically layered media. 
It is similar to the previous case,
with the notable difference that the domain is now heterogeneous in terms of hydraulic conductivity. 
This heterogeneity takes the form of different vertical layers, 
the thin red layer one in the middle being the less permeable.

\subsection*{Case 3: horizontally\_layered\_confined.h5}
In this case we reproduce a scenario similar to  the previous one
with heterogeneity taking the form of horizontal layers. 
The lowermost layer has the highest permeability.

\subsection*{Case 4: arbitrary\_layered\_1.h5}
Case 4 presents a confined flow in an arbitrary heterogeneous media. 
Using the same boundary conditions as in previous examples, 
we set up a poorly conductive material with a lence of highly permeable sediment in it. 
See how water seeks itself towards the highly permeable material, 
taking longer, but faster routes through it. 
See also how it even affects flowlines outsides the linse. 
How do you explain it?

\subsection*{Case 5: arbitrary\_layered\_2.h5}
In this example, we did not used a difference in head to trigger water flow. 
We decided an entry and exit flow at two different points of the domain. 
Remember that mass is conserved, for the numerical solver to work we need to inject as much water in the system as is discharged. 
In Visual Pyflow, Users are prompted to enter specific discharge, 
that means that flow is dependent of the size of the cell edge. 
Think of it when you create scenario. To avoid confusion, 
this domain is meshed with square cells. 
We chose a realistic specific discharge of 1.59*10-8 m/s, corresponding to 500 mm/y.  
The permeability here is strongly heterogeneous, 
with two types of material with very different permeabilities thrusted into each others. 
See how water is being channelized by the most conductive media.

\subsection*{Case 6: space\_invaders.h5 (for advanced users)}



\bibliography{user_manual.bib}{}
\bibliographystyle{apalike}
\end{document}          
