preconditioners Package
=======================

:mod:`~fipy.preconditioners` Package
------------------------------------

.. automodule:: fipy.solvers.pysparse.preconditioners
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.pysparse.preconditioners.jacobiPreconditioner` Module
-------------------------------------------------------------------------

.. automodule:: fipy.solvers.pysparse.preconditioners.jacobiPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.pysparse.preconditioners.preconditioner` Module
-------------------------------------------------------------------

.. automodule:: fipy.solvers.pysparse.preconditioners.preconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.pysparse.preconditioners.ssorPreconditioner` Module
-----------------------------------------------------------------------

.. automodule:: fipy.solvers.pysparse.preconditioners.ssorPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

