preconditioners Package
=======================

:mod:`~fipy.preconditioners` Package
------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.domDecompPreconditioner` Module
----------------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.domDecompPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.icPreconditioner` Module
---------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.icPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.jacobiPreconditioner` Module
-------------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.jacobiPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.multilevelDDMLPreconditioner` Module
---------------------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.multilevelDDMLPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.multilevelDDPreconditioner` Module
-------------------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.multilevelDDPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.multilevelNSSAPreconditioner` Module
---------------------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.multilevelNSSAPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.multilevelSAPreconditioner` Module
-------------------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.multilevelSAPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.multilevelSGSPreconditioner` Module
--------------------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.multilevelSGSPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.multilevelSolverSmootherPreconditioner` Module
-------------------------------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.multilevelSolverSmootherPreconditioner
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.solvers.trilinos.preconditioners.preconditioner` Module
-------------------------------------------------------------------

.. automodule:: fipy.solvers.trilinos.preconditioners.preconditioner
    :members:
    :undoc-members:
    :show-inheritance:

